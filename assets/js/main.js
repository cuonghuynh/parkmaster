$(document).ready(function() {

	new WOW().init();

	showSlider();

	ajaxSubmit();
	
});

function ajaxSubmit()
{
	$('#contact-form').submit(function(event) {

		//prevent default posting of form
		event.preventDefault();

		$('.modal').addClass('animated fadeIn show');

		$('#captcha').removeClass('animated shake fail');
		$('#name').removeClass('animated shake fail');
		$('#email').removeClass('animated shake fail');
		$('#company').removeClass('animated shake fail');
		$('#contact').removeClass('animated shake fail');
		$('#message').removeClass('animated shake fail');

		var dt = $(this).serialize();
		$.ajax({
			url: 'api_sendmail.php',
			type: 'POST',
			dataType: 'json',
			data: dt,
		})
		.done(function(res) {
			
			if (res === 'true') {

				alert('Email sent successfully.');
				$('#captcha').val('');
				$('#name').val('');
				$('#email').val('');
				$('#company').val('');
				$('#contact').val('');
				$('#message').val('');

			} else {

				if ( res === 'false' ) {
					alert('Email can\'t send right now, please try again. ');
				} else {

					if ( res['captcha'] === undefined ) {
						$('#captcha').addClass('animated shake fail');
					}

					if ( res['name'] === undefined ) {
						$('#name').addClass('animated shake fail');
					} 

					if ( res['email'] === undefined ) {
						$('#email').addClass('animated shake fail');
					} 

					if ( res['company'] === undefined ) {
						$('#company').addClass('animated shake fail');
					} 

					if ( res['contact'] === undefined ) {
						$('#contact').addClass('animated shake fail');
					} 

					if ( res['message'] === undefined ) {
						$('#message').addClass('animated shake fail');
					} 

				}
			}

			$('.modal').removeClass('animated fadeIn show');
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.error( "The following error occurred: "+ textStatus, errorThrown );
		});

	});
}

function showSlider()
{
	var _current = 1;
	var _next = 1;
	$("ul.slider > li:gt(0)").css('opacity', 0);

	setInterval(function() {
		
		_next = _current + 1;

		if (_next > $('ul.slider li').length) {
			_next = 1;
		};

		_str = "ul.slider li:nth-child(" + _next + ")";
		$(_str).animate({opacity: 1.0}, 2000);
		_str = "ul.slider li:nth-child(" + _current + ")";
		$(_str).animate({opacity: 0.0}, 2000);

		_current = _next;

	}, 7000);

}
