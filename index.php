<?php 
session_start();

require 'api/simple-captcha/simple-php-captcha.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<title>Park Master</title>

	<!-- Documentation extras -->
	<link href="style.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="assets/css/animate.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->



</head>
<?php

	$_SESSION = array();
	$_SESSION['captcha'] = simple_php_captcha();

?>
<body>
	<div class="modal">
		
	</div>
	<div id="site-wrapper"> <!-- begin site-wrapper -->
		<div id="our-product"> <!-- begin our-product -->
			<div class="op-1 dark-bg">
				<a href="/" ><img class="logo wow bounceInLeft" src="assets/images/logo.png" alt="Paker Master"></a>
				<p class="robo description wow bounceInRight">“We offer solutions to any parking environment including AutoPay system, on and <br/>off-street Pay & display, Automatic Number Plate Recognition, Parking Guidance <br/>System and Security Products.”</p>
				<ul class="slider">
					<li>
						<div class="slider-image" style="background-image: url('assets/images/slide-1.jpg')"></div>
					</li>
					<li>
						<div class="slider-image" style="background-image: url('assets/images/slide-2.jpg')"></div>
					</li>
					<li>
						<div class="slider-image" style="background-image: url('assets/images/slide-3.jpg')"></div>
					</li>
				</ul>
			</div>
			<div class="op-2 white-bg">
				<div class="robo heading wow bounceInLeft">Our Product</div>
				<p class="content wow bounceInRight">Park Master are an alliance of three leading manufacturers of parking automation, revenue control systems <br/>and pay and display machines.</p>
			</div>
		</div> <!-- end our-product -->
		<div id="autopay-system" class="dark-bg container"> <!-- begin autopay-system -->
			<div class="robo heading wow bounceInLeft">AutoPay System</div>
			<p class="description wow bounceInRight">Par Master's newest AutoPay System range, enables car parks to e managed <br/>with minimal investment and maximised revenue.</p>
			<p class="content wow bounceInRight">The success of the system is based on an efficent network of terminals linked to a central management <br/>system run by powerful software to administer the whole system.</p>
			<p class="rale download title wow bounceInLeft">Download Catalogue:</p>
			<div class="download content wow flipInX">
				<ul>
					<li><a href="uploads/docs/Autopay Compact.pdf"><i class="btn-download">Compact</i></a></li>
					<li>
						<i class="btn-download">Elegence</i>
						<ul>
							<li><a href="uploads/docs/Autopay Elegance - Entry and Exit System.pdf" class="rale">Entry & Exit Machines</a></li>
							<li><a href="uploads/docs/Autopay Elegance - Pay System.pdf" class="rale">Pay Station</a></li>
							<li><a href="uploads/docs/Autopay Elegance - Central Management System.pdf" class="rale">Management System</a></li>
							<li><a href="uploads/docs/Autopay Elegance - Rising Arm Bar.pdf" class="rale">Rising Arm Barrier</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div> <!-- end autopay-system -->
		<div id="pay-display" class="white-bg container"> <!-- begin pay-display -->
			<div class="robo heading wow bounceInLeft">Pay and Display</div>
			<p class="description wow bounceInRight">Our Pay & Display systems are versatile and adaptable, designed to meet the <br/> significant demands of the Pay and Display environments.</p>
			<p class="content wow bounceInRight">Where payment flexibility is essential, our Pay & Display systems are one of the few on the worldwide market that offers change <br/>giving, bank note reading and Chip & PIN credit card facility.</p>
		</div> <!-- end pay-display -->
		<div id="parking-guidance" class="dark-bg container"> <!-- begin parking-guidance -->
			<div class="robo heading wow bounceInLeft">Parking Guidance</div>
			<p class="description wow bounceInRight">Park Master’s Optima guidance system has been designed to improve traffic flow <br/>significantly within car parks. Colour coded indicators provide accurate <br/>information to drivers of space occupancy in each bay.</p>
			<p class="content wow bounceInRight">With its many advantages, the Optima system provides a wealth of reports, assisting users with useful information <br/>on occupancy, sector usage and individual space usage.</p>
			<div class="robo heading wow bounceInLeft">Advantages</div>
			<p class="content wow bounceInRight">Increases vehicle car park throughput, thus allowing greater traffic flow, providing faster space occupancy and maximising revenue</p>
			<div class="col-1 wow bounceInLeft">
				<div class="content">
					<ul>
						<li>Energy saving: by guiding vehicles to available spaces it is feasible to control parking levels more efficiently. Floor by floor illumination can be controlled such that lighting is only provided on levels that are occupied.</li>
						<li>Staff deployment: as Optima needs minumal human input, operator personnel can be deployed in other areas, therby reducing operating costs.</li>
						<li>The rapid detection of free spaces reduces customer stress and allows them to focus on the purpose of their visit. Convenience and efficient use of time are key factors for continued customer loyalty.</li>
					</ul>
				</div>
			</div>
			<div class="col-2 wow bounceInRight">
				<div class="content">
					<ul>
						<li>Fully scalable system which can expand with future requirements. I can be easilly integrated with any existing car park control system.</li>
						<li>Designed around a 'low maintenance' philosophy and does not require any specialist support.</li>
						<li>The statistiscs generated by Optima, both numerical and graphical, allow operators to make educated decisions for their parking facilities.</li>
					</ul>
				</div>
			</div>
		</div> <!-- end parking-guidance -->
		<div id="anpr" class="white-bg container"> <!-- begin anpr -->
			<div class="robo heading wow bounceInLeft">Automatic Number Plate Recognition</div>
			<p class="content wow bounceInRight">Parmaster Optipark is one of the most technically advanced and secure parking systems available.</p>
			<p class="content wow bounceInRight">It is especially suited to large sites and sensitive areas, providing instantaneous recognition of vehicle registration plates, to discern <br/>between authorised and unauthorised users.</p>
			<p class="content wow bounceInRight">Optipark can be seamlessly integrated with Parkare’s revenue and security systems and can be customised to suit specific customer <br/>requirements.</p>
			<div class="robo heading wow bounceInLeft">Features</div>
			<div class="col-1 wow bounceInLeft">
				<div class="content">
					<ul>
						<li>'Hot Lists' alerts</li>
						<li>Ideal for staff parking and VIP control</li>
						<li>Entry/exit transaction log with time/date and owner ID</li>
						<li>Alerts to vehicles exceeding user defined stay periods</li>
					</ul>
				</div>
			</div>
			<div class="col-2 wow bounceInRight">
				<div class="content">
					<ul>
						<li>Revenue collection integrated with Parkare products</li>
						<li>Ticketless revenue collection</li>
						<li>Vehicle Number Plate data encoded on Easi2 ticket</li>
						<li>Additional cameras for supplementary images</li>
					</ul>
				</div>
			</div>
		</div> <!-- end anpr -->
		<div id="contact-us" class="container dark-bg wow fadeInUp"> <!-- begin contact-us -->
			<div class="robo heading">Contact Us</div>
			<p class="content">Write to us to know anything about idea products</p>
			<form class="cf lato" id="contact-form">
				<div class="col-1">
					<input type="text" placeholder="Name" name="name" id="name" value="" required minlength="3" >
					<input type="email" placeholder="Email Address" name="email" id="email" value="" required >
				</div>
				<div class="col-2">
					<input type="text" placeholder="Company Name" name="company" id="company" minlength="3" value="<?php if ( isset($mail) && !isset($mail->errors['company']) ) { echo $mail->oldInput['company']; } ?>" <?php if ( isset($mail) && (count($mail->errors) > 0) && isset($mail->errors['company']) ) { echo 'class="fail"'; } ?> required >
					<input type="tel" placeholder="Contact Number" name="contact" id="contact" value="<?php if ( isset($mail) && !isset($mail->errors['contact']) ) { echo $mail->oldInput['contact']; } ?>" <?php if ( isset($mail) && (count($mail->errors) > 0) && isset($mail->errors['contact']) ) { echo 'class="fail"'; } ?> required >
				</div> 
				<textarea class="lato" name="message" id="message" placeholder="Message" rows="4" minlength="5" required ></textarea>
				<div class="bottom-wrapper">
					<p class="capcha-validation">
						<span class="content">Capcha Code</span>
						<input type="text" name="captcha" id="captcha" value="" <?php if ( isset($mail) && (count($mail->errors) > 0) && isset($mail->errors['captcha']) ) { echo 'class="fail"'; } ?>>
						<input type="hidden" name="code" id="code" value="<?php echo $_SESSION['captcha']['code']; ?>">
						<img class="captcha-image" src="<?php echo $_SESSION['captcha']['image_src']; ?>" alt="CAPTCHA code">
					</p>
					<input type="submit" value="SEND MESSAGE">
				</div>
			</form>
		</div> <!-- end contact-us -->
		<div id="contact-no" class="blue-bg container wow fadeInUp"> <!-- begin contact-no -->
			<p class="content">Park Master Sdn Bhd(250613-P) <br/>No 3-2, 3rd Floor, Dataran Pelangi Utama, <br/>Jln Masjig PJU 6A, 47400 Petaling Jaya, Selangor.</p>
			<p class="content">Tel: +603-7727 3077 <br/>Fax: +603-7726 2718</p>
		</div> <!-- end contact-no -->
		<div id="copyright" class="white-bg container wow fadeInUp">
			<p class="content">© 2015, Park Master Sdn Bhd . All Rights Reserved</p>
		</div>
	</div> <!-- end site-wrapper -->
	
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="assets/js/main.js"></script>
	<script src="assets/js/wow.min.js"></script>
	
</body>
</html>