<?php 

require 'api/sendmail.php';

if (is_ajax()) {

	$errors = array();
	$oldInput = array();

	if ( !empty($_POST) ) {

		//validate form
		$name = $_POST['name'];
		$email = $_POST['email'];
		$company = $_POST['company'];
		$contact = $_POST['contact'];
		$message = $_POST['message'];

		$code = $_POST['captcha'];
		$secret = $_POST['code'];

		if ( empty($name) ) {
			$errors['name'] = 1;
		} else {
			$oldInput['name'] = $name;
		}

		if ( empty($email) ) {
			$errors['email'] = 1;
		} else {
			$oldInput['email'] = $email;
		}

		if ( empty($company) ) {
			$errors['company'] = 1;
		} else {
			$oldInput['company'] = $company;
		}

		if ( empty($contact) ) {
			$errors['contact'] = 1;
		} else {
			$oldInput['contact'] = $contact;
		}

		if ( empty($message) ) {
			$errors['message'] = 1;
		} else {
			$oldInput['message'] = $message;
		}

		if ( $code != $secret ) {
			$errors['captcha'] = 1;
		} else {
			$oldInput['captcha'] = "true";
		}

		if ( count($errors) == 0 && $oldInput['captcha'] == "true") {

			$mail = new Mail( 587 ,
				'mail.iconcept.com.my', 
				'tls', 
				'duc.nguyen@iconcept.com.my', 
				'duc.nguyen123!@#', 
				'Duc Nguyen', 
				'cuong.huynh@eastbayhub.com', 
				'Park Master' );

			if ( !$mail->send('Contact Form', $name, $email, $company, $contact, $message) ) {
				echo json_encode('false');
			} else {
				echo json_encode('true');
			}

		} else {
			echo json_encode($oldInput);
		}

	}

}


//Function to check if the request is an AJAX request
function is_ajax() {
  	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

 ?>